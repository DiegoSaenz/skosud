# README #

El proyecto a SKOSUD puede ser accedido desde la siguiente dirección en su versión online: [http://skosud.azurewebsites.net/Front/index.html](Link URL) , y los fuentes que se encuentran en este repositorio corresponden a el trabajo realizado utilizando librerias Javascript y Html5, Bootstrap, Angular, entre otras para el presente proyecto.

![cap2.png](https://bitbucket.org/repo/nzj5eB/images/2936710338-cap2.png)
![cap1.png](https://bitbucket.org/repo/nzj5eB/images/607234001-cap1.png)

### Objetivo del repositorio ###

* El Repositorio busca ser un soporte documental y técnico para su seguimiento y desarrollos posteriores.
* Version 1.0


### Objetivo del Proyecto ###

* Plantear un modelo de mecanismos de búsquedas navegacionales a partir de esquemas de representación de conocimiento para el acceso a recursos digitales en abierto. Para el desarrollo del objetivo principal se plantean los siguientes objetivos específicos:

* •Identificación de fuentes de recursos en abierto para su vinculación en procesos de búsqueda navegacional.
* •Analizar estructuras de esquemas de representación de conocimiento que faciliten la integración de recursos digitales asociados a temáticas de recursos en abierto seleccionados.
* •Identificar mecanismos de acceso visual para la integración de esquemas de representación de conocimiento como interfaces de búsqueda navegacional. 
* •Definir un componente de búsqueda navegacional para la búsqueda de recursos digitales en abierto a partir de esquemas de representación del conocimiento.
* •Llevar a cabo pruebas de usabilidad de interfaces de búsqueda visual implementadas para identificar aquellas más efectivas dentro de procesos de búsqueda y localización de recursos.