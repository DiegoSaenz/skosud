var superj=window.parent.GetSuperJson();
console.log("casa",superj);
var todos = angular.module('todos', ['ui.bootstrap']);
var mm;
todos.controller('TodoController', function($scope) {
   $scope.filteredTodos = []
  ,$scope.currentPage = 1
  ,$scope.numPerPage = 3
  ,$scope.maxSize = 3;
  
  $scope.makeTodos = function() {
    $scope.todos = [];
    for (i=0;i<superj.length;i++) {
      var image="";
      if(superj[i].hasOwnProperty('edmPreview')){
        image=superj[i].edmPreview[0];
      }
      ////////////////////////////////////////////////////////       
      var descript="";
      if(superj[i].hasOwnProperty('dcDescription')){
        descript=superj[i].dcDescription[0].substring(0, 350);
      }
      
      ////////////////////////////////////////////////////////
      $scope.todos.push({ titulo:superj[i].title[0]
        ,link:superj[i].guid,
        preview:image,
        description:descript,
        done:false});
    }
  };
  $scope.makeTodos(); 
  
  $scope.numPages = function () {
    return Math.ceil($scope.todos.length / $scope.numPerPage);
  };
  
  $scope.$watch('currentPage + numPerPage', function() {
    var begin = (($scope.currentPage - 1) * $scope.numPerPage)
    , end = begin + $scope.numPerPage;
    
    $scope.filteredTodos = $scope.todos.slice(begin, end);
  });
});

